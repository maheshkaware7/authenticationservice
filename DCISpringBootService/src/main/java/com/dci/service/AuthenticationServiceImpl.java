package com.dci.service;

import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dci.ExceptionHandler.UserNotFoundException;
import com.dci.dao.ServiceDao;
import com.dci.model.Group;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
	private static final Logger LOGGER = LogManager.getLogger(AuthenticationServiceImpl.class);
	@Autowired
	private ServiceDao dao;
	
	
    
	@Override
	public Map authenticateUser(String userId, String passWord)throws UserNotFoundException {
		LOGGER.info(
				"Entering into com.dci.service.AuthenticationServiceImpl.authenticateUser(String userId, String passWord)");
		Map userSaltMap = dao.getUserSaltDetails(userId);
		String userSaltValue = (String) userSaltMap.get("FSALT");
		String userDigest = "";

		if (userSaltValue != null && !userSaltValue.trim().equals("")) {
			UserAuthenticationDigest userAuthenticationDigest = new UserAuthenticationDigest();
			userDigest = userAuthenticationDigest.getUserDigest(userId, passWord, userSaltValue);
			return dao.authenticateUserDB(userId, userDigest);

		} else if (userSaltValue != null && userSaltValue.trim().equals("")) {
			// This block is when user salt is not found i.e user not found
			Map authenticationDetails = new HashMap();
			authenticationDetails.put("HFMSGID", "3");
			authenticationDetails.put("HFUSERID", "0");
			authenticationDetails.put("HFMSG", "");
			return authenticationDetails;
		}
		LOGGER.info(
				"Exiting from com.dci.service.AuthenticationServiceImpl.authenticateUser(String userId, String passWord)");
		return null;

	}

	

}
