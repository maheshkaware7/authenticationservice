package com.dci.service;

import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jasypt.contrib.org.apache.commons.codec_1_3.binary.Base64;
import org.jasypt.digest.StandardStringDigester;
import org.jasypt.salt.RandomSaltGenerator;
import org.jasypt.salt.StringFixedSaltGenerator;

public class UserAuthenticationDigest {
	private static final Logger LOGGER = LogManager.getLogger(UserAuthenticationDigest.class);
	private HashMap departmentConfig;
	// DciCommon dcicom = new DciCommon();

	final static int saltSize = 24;
	/*
	 * private String algorithm = ""; private String iterations = "";
	 */

	public String saltGenerator() {
		LOGGER.info("Entering in the com.dci.service.UserAuthenticationDigest.saltGenerator()");
		RandomSaltGenerator randomSaltGenerator = new RandomSaltGenerator();
		byte[] randomSalt = randomSaltGenerator.generateSalt(saltSize);
		// String randomSaltStr = new String(randomSalt);
		randomSalt = Base64.encodeBase64(randomSalt);
		String randomSaltStr = new String(randomSalt);
		// return randomSaltStr;
		LOGGER.info("Exisiting from com.dci.service.UserAuthenticationDigest.saltGenerator()");
		return randomSaltStr;
	}

	public StandardStringDigester getDigester(String saltValue) {
		LOGGER.info("Entering in the com.dci.service.UserAuthenticationDigest.getDigester()");

		String algorithmValue = "SHA-512";
		String iterationsValue = "50000";
		// String actualAlgorithm =
		// com.dci.ComponentInfo.utilities.Base64CoderUtil.decodeString(algorithmValue);
		// String actualIterations =
		// com.dci.ComponentInfo.utilities.Base64CoderUtil.decodeString(iterationsValue);
		String actualAlgorithm = algorithmValue;
		String actualIterations = iterationsValue;
		StandardStringDigester digester = new StandardStringDigester();
		StringFixedSaltGenerator fixedSaltValue = new StringFixedSaltGenerator(saltValue);
		digester.setAlgorithm(actualAlgorithm);
		digester.setIterations(Integer.parseInt(actualIterations));
		digester.setSaltGenerator(fixedSaltValue);
		LOGGER.info("Exisiting from com.dci.service.UserAuthenticationDigest.getDigester()");
		return digester;

	}

	public String digester(String userPassword, String salt) {
		LOGGER.info("Entering in the com.dci.service.UserAuthenticationDigest.digester()");
		StandardStringDigester digester = getDigester(salt);
		if (digester != null) {
			String digest = digester.digest(userPassword);
			LOGGER.info("Exisiting from com.dci.service.UserAuthenticationDigest.digester()");
			return digest;
		} else {
			LOGGER.info("Exisiting from com.dci.service.UserAuthenticationDigest.digester()");
			return "";
		}

	}

	public String getUserDigestValue(String inputSaltPassword, String salt) {
		LOGGER.info("Entering in the com.dci.service.UserAuthenticationDigest.getUserDigestValue()");
		StandardStringDigester digester = getDigester(salt);
		if (digester != null) {
			String digest = digester.digest(inputSaltPassword);

			LOGGER.info("Exisiting from com.dci.service.UserAuthenticationDigest.getUserDigestValue()");
			return digest;
		} else {
			LOGGER.info("Exisiting from com.dci.service.UserAuthenticationDigest.getUserDigestValue()");

			return "";
		}

	}

	public String getUserDigest(String userId, String password, String salt) {
		LOGGER.info("Entering in the com.dci.service.UserAuthenticationDigest.getUserDigest()");
		// get the users salt from the database with help of userID
		if (salt != null && !salt.trim().equals("")) {
			String saltedPassword = salt + password;
			String digest = getUserDigestValue(saltedPassword, salt);
			LOGGER.info("Exisiting from com.dci.service.UserAuthenticationDigest.getUserDigest()");
			return digest;
		} else {
			LOGGER.info("Exisiting from com.dci.service.UserAuthenticationDigest.getUserDigest()");
			return "noUser";
		}
	}

	public HashMap<String, String> newUserDigest(String password) {
		LOGGER.info("Entering in the com.dci.service.UserAuthenticationDigest.newUserDigest()");
		String newSalt = saltGenerator();
		String saltedPassword = newSalt + password;
		String digest = digester(saltedPassword, newSalt);
		HashMap<String, String> saltMap = new HashMap<String, String>();
		saltMap.put("SALT", newSalt);
		saltMap.put("DIGEST", digest);
		LOGGER.info("Exisiting from com.dci.service.UserAuthenticationDigest.newUserDigest()");
		return saltMap;

	}

	public String getUsersPreviousDigestString(List<String> previousUsedSalts, String inputPassword) {
		LOGGER.info("Entering in the com.dci.service.UserAuthenticationDigest.getUsersPreviousDigestString()");
		String concatenatedDigests = "";
		String SEPERATOR = "$#$#$#$#$#";
		for (String saltValue : previousUsedSalts) {
			String saltPassword = saltValue + inputPassword;
			String digest = digester(saltPassword, saltValue);
			// developerLog.debug("digest \n"+digest+"\n");
			concatenatedDigests = concatenatedDigests + SEPERATOR + digest;
		}
		if (concatenatedDigests.startsWith(SEPERATOR)) {
			concatenatedDigests = concatenatedDigests.substring(SEPERATOR.length());
		}
		// developerLog.debug("concatenatedDigests \n"+concatenatedDigests+"\n");
		LOGGER.info("Exisiting from com.dci.service.UserAuthenticationDigest.getUsersPreviousDigestString()");
		return concatenatedDigests;
	}
}
