package com.dci.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.session.data.redis.config.ConfigureRedisAction;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;


@Configuration
@EnableRedisHttpSession
public class SessionConfig extends AbstractHttpSessionApplicationInitializer{

	@Bean
    public JedisConnectionFactory connectionFactory() {
        return new JedisConnectionFactory();
    }
	
	 @Bean 
	 public static ConfigureRedisAction configureRedisAction()
	 { 
		 return ConfigureRedisAction.NO_OP; 
		 
	 } 
	 
	 
	 
	
	
	@Bean
	public RedisTemplate<String, Object> redisTemplate() {
		final RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
		template.setConnectionFactory(connectionFactory());	
		template.setValueSerializer(new GenericToStringSerializer<Object>(Object.class));
		return template;
	}
}
