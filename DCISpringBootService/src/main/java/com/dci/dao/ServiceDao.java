package com.dci.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.dci.DciSpringBootServiceApplication;
import com.dci.ExceptionHandler.UserNotFoundException;
import com.dci.model.Group;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;


@Repository
public class ServiceDao  {
	private static final Logger LOGGER = LogManager.getLogger(ServiceDao.class);

	private SimpleJdbcCall jdbcCall;
	@Autowired
	private DataSource dataSource;
	
	@Autowired
    private JdbcTemplate jdbcTemplateObject;
	
	@Autowired
	private Environment environment;

	Map authenticationDetails = new HashMap();
	
	
	private Connection getConnection()
	{
		Connection con=null;
		try {
			con = jdbcTemplateObject.getDataSource().getConnection();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return con;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, String> getUserSaltDetails(String userId)  {
		LOGGER.info("Entering into com.dci.dao.ServiceDao.getUserSaltDetails(String userId)");
		Map<String, String> Saltmap = new HashMap<String, String>();
		
		Connection con=null;
		ResultSet rs=null;
		CallableStatement cs=null;
		
		LOGGER.info("call SP7_DSLOGIN(" + userId + ")");
		try {
			LOGGER.info("call SP7_DSGETSALT (userId)");
			con=getConnection();
			cs=con.prepareCall("{call SP7_DSGETSALT(?)}");
			cs.setString(1, userId);
			cs.execute();
			rs=cs.getResultSet();
			 while(rs.next())
        	 {
        		 Saltmap.put("FUSERID",rs.getString("FUSERID"));
        		 Saltmap.put("FRESET_IND",rs.getString("FRESET_IND"));
        		 Saltmap.put("FACCOUNT_STATUS",rs.getString("FACCOUNT_STATUS"));
        		 Saltmap.put("FSALT",rs.getString("FSALT"));
        	 }
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		finally {
			try {
				if (con != null)
					con.close();
				if (rs != null)
					rs.close();
				if (cs != null)
					cs.close();
			} catch(SQLException e) {}
		}
		
	   LOGGER.info("Exiting from com.dci.dao.ServiceDao.getUserSaltDetails(String userId)");
		return Saltmap;
	}

	
	public String getUserSalt(String userId) {
		LOGGER.info("Entering into com.dci.dao.ServiceDao.getUserSalt(String userId)");
		String salt = "";
		Map userSaltMap = getUserSaltDetails(userId);
		if (userSaltMap.containsKey("FSALT")) {
			salt = (String)userSaltMap.get("FSALT");
		}
		LOGGER.info("Existing from com.dci.dao.ServiceDao.getUserSalt(String userId)");
		return salt;
	}

	@SuppressWarnings("unchecked")
	public Map authenticateUserDB(String username, String passwordSalt){
		LOGGER.info("Entering into com.dci.dao.ServiceDao.authenticateUserDB(String username, String passwordSalt)");
		Connection con=null;
		ResultSet rs=null;
		CallableStatement cs=null;
	
		try {
			con=getConnection();
			LOGGER.info("call SP7_DSLOGIN(" + username + "," + passwordSalt + ")");
			cs=con.prepareCall("{CALL SP7_DSLOGIN(?,?,?,?,?)}");
			cs.setString(1, username);
			cs.setString(2, passwordSalt);
			cs.registerOutParameter(3, Types.INTEGER);
			cs.registerOutParameter(4, Types.INTEGER);
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.executeUpdate();
			int msgId = cs.getInt(3);
			int userId = cs.getInt(4);
			String msg = cs.getString(5);
			
			  
			
				authenticationDetails.put("HFUSERID", userId);
				authenticationDetails.put("HFMSG", msg);
				authenticationDetails.put("HFMSGID", msgId);
	
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		finally {
			try {
				if (con != null)
					con.close();
				if (rs != null)
					rs.close();
				if (cs != null)
					cs.close();
			} catch(SQLException e) {}
		}
		
		LOGGER.info("Exisiting from com.dci.dao.ServiceDao.authenticateUserDB(String username, String passwordSalt)");
		return authenticationDetails;
	}
	
    @SuppressWarnings("unchecked")
	public Map<String, String> getClientInfo(String userId, String clientId) throws UserNotFoundException {
		LOGGER.info("Entering into com.dci.dao.ServiceDao.getClientInfo(String userId, String clientId)");
		Connection con=null;
		ResultSet rs=null;
		CallableStatement cs=null;
		Map<String, String> clientInfoMap = new HashMap<String, String>();
		
		try {
			
			con=getConnection();
			cs=con.prepareCall("{CALL SP8_GETCLIENTINFOVALIDATE888(?,?,?)}");
			cs.setString(1, userId);
			cs.setString(2, clientId);
			cs.registerOutParameter(3, Types.VARCHAR);
			cs.execute();
			rs=cs.getResultSet();
			 String msg=cs.getString("HFMSG");
			 if(msg.contains("INVALID"))
			 {
				    clientInfoMap.put("HFMSG", msg);  
				    throw new UserNotFoundException("User Not Found/Invalid User");
			 }
			 else
			 {
				  String message=cs.getString("HFMSG");
				 
				 while(rs.next())
				 {
					    clientInfoMap.put("FDEPT_NAME", rs.getString("FDEPT_NAME"));
	        			clientInfoMap.put("FDEPTID", rs.getString("FDEPTID"));
	        			clientInfoMap.put("FAPPID", rs.getString("FAPPID"));
	        			clientInfoMap.put("HFMSG", message);
				 } 
				 
			 }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		finally{
			
			try {
				if (con != null)
					con.close();
				if (rs != null)
					rs.close();
				if (cs != null)
					cs.close();
			} catch(SQLException e) {}
			
		}
		
		    
		   return clientInfoMap;
    }	   
		
	public Object getClientList() {
		try {
			LOGGER.info("Entering into com.dci.dao.ServiceDao.getClientList()");
			Object value = null;

			jdbcCall = new SimpleJdbcCall(dataSource).withSchemaName("ZDBXLM0005").withProcedureName("SP_UTGETCLIENTLIST").declareParameters(new SqlParameter("HFUSER_ID", Types.VARCHAR));
			Map<String, Object> inParamMap = new HashMap<String, Object>();
			inParamMap.put("HFUSER_ID", "dcisupportlm");
			SqlParameterSource in = new MapSqlParameterSource(inParamMap);
			Map<String, Object> out = jdbcCall.execute(in);
			Iterator<Entry<String, Object>> it = out.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, Object> entry = (Map.Entry<String, Object>) it.next();
				String key = (String) entry.getKey();
				value = (Object) entry.getValue();
			}
			LOGGER.info("Exisiting from com.dci.dao.ServiceDao.getClientList()");
			return value;		
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	 @SuppressWarnings("unchecked")
	public Object getPrivileges(String userId,String userName,String clientId){
		 
	    	LOGGER.info("Entering into com.dci.service.ServiceDao.getPrivileges(String userId, String clientId)");
	    	Connection con=null;
			ResultSet rs=null;
			CallableStatement cs=null;
	  
	    	   Group group=new Group(clientId,userName);
	    	
	    	  HashMap<String,Boolean> TextMap=new HashMap();
		      HashMap<String,Boolean> SubheadMap=new HashMap();
		      HashMap<String,Boolean> FootnoteMap=new HashMap();
		      HashMap<String,Boolean> ImageMap=new HashMap();
		      HashMap<String,Boolean> TableMap=new HashMap();
		      HashMap<String,Boolean> TitleMap=new HashMap();
		      HashMap<String,Boolean> FootNote_MappingMap=new HashMap();
		      HashMap<String,Boolean> Fund_ManagementMap=new HashMap();
		      HashMap<String,Boolean> VariableMap=new HashMap();
		   /*   
		      HashMap<String,Boolean> SaiMap=new HashMap();
		      HashMap<String,Boolean> Prospectus_WellsMap=new HashMap();
		      HashMap<String,Boolean> ProxyMap=new HashMap();
		      HashMap<String,Boolean> WrapperMap=new HashMap();
		      HashMap<String,Boolean> Summary_Prospectus_WellsMap=new HashMap();
		     */
		      HashMap<String,String> DocTypes=new HashMap();
		      HashMap<String,HashMap<String,Boolean>> DocOutputTypes=new LinkedHashMap();  
		      
		      
		      
				try {
					con=getConnection();
					LOGGER.info("call SP7_DSGETUSERPERMISION(String userId,String userName,String clientId)");
					cs=con.prepareCall("{call SP7_DSGETUSERPERMISION(?,?,?)}");
					cs.setString(1, userId);
					cs.setString(2, clientId);
					cs.setString(3, environment.getProperty("application.docubuilderId"));
					cs.execute();
					rs=cs.getResultSet();
					
							while(rs.next()){
								
							 String ParentId=rs.getString("FDCIOBJECT_PARENTID");
							 String PrivilegeDesc=rs.getString("FPRIV_OPTION_DESC");
							
							 boolean SelectedInd=false;
							 
		                     /*Setting SelectedInd true according to different access permissions */
							 if(rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE")||rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT")||rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW")||rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("Yes")||rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("Selected"))
								 {
									 SelectedInd=true;
								 }
							 
							  /* Filling 1st HashMap with DocumentTypes as key and Its ID as value and 2nd HashMap filling with DocumentType as Key and new HashMap as value */
							 if(rs.getString("FOBJSCHEMA_DESC").equalsIgnoreCase("Document Types"))
								{
									 
									DocTypes.put(rs.getString("FDCIOBJECT_DESC"),rs.getString("FDCIOBJECTID"));
									DocOutputTypes.put(rs.getString("FDCIOBJECT_DESC"),new HashMap<String,Boolean>());
								}
									
							/* putting access permissions according to component type */
							if (rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Text")&& SelectedInd) {
		
								if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE") && SelectedInd) {
									TextMap.put("CREATE", true);
									group.setText(TextMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT") && SelectedInd) {
									TextMap.put("EDIT", true);
									group.setText(TextMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW") && SelectedInd) {
									TextMap.put("VIEW", true);
									group.setText(TextMap);
								}
							} else if (rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Subhead")&& SelectedInd) {
		
								if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE") && SelectedInd) {
									SubheadMap.put("CREATE", true);
									group.setSubhead(SubheadMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT") && SelectedInd) {
									SubheadMap.put("EDIT", true);
									group.setSubhead(SubheadMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW") && SelectedInd) {
									SubheadMap.put("VIEW", true);
									group.setSubhead(SubheadMap);
								}
							} else if (rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Footnote") && SelectedInd) {
		
								if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE") && SelectedInd) {
									FootnoteMap.put("CREATE", true);
									group.setFootnote(FootnoteMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT") && SelectedInd) {
									FootnoteMap.put("EDIT", true);
									group.setFootnote(FootnoteMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW") && SelectedInd) {
									FootnoteMap.put("VIEW", true);
									group.setFootnote(FootnoteMap);
								}
							}
							else if (rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Image") && SelectedInd) {
		
								if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE") && SelectedInd) {
									ImageMap.put("CREATE", true);
									group.setImage(ImageMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT") && SelectedInd) {
									ImageMap.put("EDIT", true);
									group.setImage(ImageMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW") && SelectedInd) {
									ImageMap.put("VIEW", true);
									group.setImage(ImageMap);
								}
							}
		
							else if (rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Title") && SelectedInd) {
		
								if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE") && SelectedInd) {
									TitleMap.put("CREATE", true);
									group.setTitle(TitleMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT") && SelectedInd) {
									TitleMap.put("EDIT", true);
									group.setTitle(TitleMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW") && SelectedInd) {
									TitleMap.put("VIEW", true);
									group.setTitle(TitleMap);
								}
							} else if (rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Table") && SelectedInd) {
		
								if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE") && SelectedInd) {
									TableMap.put("CREATE", true);
									group.setTable(TableMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT") && SelectedInd) {
									TableMap.put("EDIT", true);
									group.setTable(TableMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW") && SelectedInd) {
									TableMap.put("VIEW", true);
									group.setTable(TableMap);
								}
							}
		
							else if (rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Footnote Mapping")
									&& SelectedInd) {
		
								if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE") && SelectedInd) {
									FootNote_MappingMap.put("CREATE", true);
									group.setFootnote_Mapping(FootNote_MappingMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT") && SelectedInd) {
									FootNote_MappingMap.put("EDIT", true);
									group.setFootnote_Mapping(FootNote_MappingMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW") && SelectedInd) {
									FootNote_MappingMap.put("VIEW", true);
									group.setFootnote_Mapping(FootNote_MappingMap);
								}
							}
		
							else if (rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Fund Management")
									&& SelectedInd) {
		
								if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE") && SelectedInd) {
									Fund_ManagementMap.put("CREATE", true);
									group.setFund_Management(Fund_ManagementMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT") && SelectedInd) {
									Fund_ManagementMap.put("EDIT", true);
									group.setFund_Management(Fund_ManagementMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW") && SelectedInd) {
									Fund_ManagementMap.put("VIEW", true);
									group.setFund_Management(Fund_ManagementMap);
								}
							}
							else if (rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Variable")
									&& SelectedInd) {
		
								if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE") && SelectedInd) {
									VariableMap.put("CREATE", true);
									group.setVariable(VariableMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT") && SelectedInd) {
									VariableMap.put("EDIT", true);
									group.setVariable(VariableMap);
								} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW") && SelectedInd) {
									VariableMap.put("VIEW", true);
									group.setVariable(VariableMap);
								}
							}
							
							else
								if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("View Library")&& SelectedInd)
								{
									group.setLibrary(true);
								}
								else
									if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Context")&& SelectedInd)
									{
										group.setContext(true);
									}
									else
										if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("All Charts Status")&& SelectedInd)
										{
											group.setCharts(true);
										}
										else
											if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Open Data File")&& SelectedInd)
											{
												group.setOpenDataFile(true);
											}
											else
												if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Upload Quantative")&& SelectedInd)
												{
													group.setUploadDataFile(true);
												}
												
												else
													if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Change Status")&& SelectedInd)
													{
															group.setChangeStatus(true);  
				    										if(rs.getString("FPRIV_OPTIONDEP_NAME").equalsIgnoreCase("inprogress"))
				    										{
				    											group.setInprogress(true);
				    										}
				    										else if(rs.getString("FPRIV_OPTIONDEP_NAME").equalsIgnoreCase("review"))
				    										{
				    											group.setReview(true);
				    										}
				    										else if(rs.getString("FPRIV_OPTIONDEP_NAME").equalsIgnoreCase("approved"))
				    										{
				    											group.setApproved(true);
				    										}
				    										else if(rs.getString("FPRIV_OPTIONDEP_NAME").equalsIgnoreCase("published"))
				    										{
				    											group.setPublished(true);
				    										}
				    										else if(rs.getString("FPRIV_OPTIONDEP_NAME").equalsIgnoreCase("revision"))
				    										{
				    											group.setRevision(true);
				    										}
				    										else if(rs.getString("FPRIV_OPTIONDEP_NAME").equalsIgnoreCase("disabled"))
				    										{
				    											group.setDelete(true);
				    										}
													    }
													else
														if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Carry Forward")&& SelectedInd)
														{
															group.setCarryForward(true);
														}
														else
															if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Proof Document")&& SelectedInd)
															{
																group.setProof(true);
															}
															else
																if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Service Desk Admin")&& SelectedInd)
																{
																	group.setServiceDesk(true);
																}
																else
																	if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Report")&& SelectedInd)
																	{
																		group.setReport(true);
																	}
																	else
																		if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Variable Management")&& SelectedInd)
																		{
																			group.setVariableManagement(true);
																		}
																		else
																			if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Supplement")&& SelectedInd)
																			{
																				group.setSupplement(true);
																			}
							
						if(rs.getString("FOBJSCHEMA_DESC").equalsIgnoreCase("Doc Output Types"))
						{    
							for(Map.Entry entry:DocTypes.entrySet())
							{
								 if(ParentId.equalsIgnoreCase((String) entry.getValue()))
							     {
									 	
									 if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("PDF")&& SelectedInd)
							    	 {
										 HashMap<String, Boolean> DocTypeMap=DocOutputTypes.get(entry.getKey());
										 DocTypeMap.put(rs.getString("FDCIOBJECT_DESC"), SelectedInd);
										 DocOutputTypes.put((String)entry.getKey(), DocTypeMap);
									 
							    	 }
									 else if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("CBPDF")&& SelectedInd)
							    	 {
										 HashMap<String, Boolean> DocTypeMap=DocOutputTypes.get(entry.getKey());
										 DocTypeMap.put(rs.getString("FDCIOBJECT_DESC"), SelectedInd);
										 DocOutputTypes.put((String)entry.getKey(), DocTypeMap);
										 
							    	 }
									 else if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("BLKPDF")&& SelectedInd)
							    	 {
										 HashMap<String, Boolean> DocTypeMap=DocOutputTypes.get(entry.getKey());
										 DocTypeMap.put(rs.getString("FDCIOBJECT_DESC"), SelectedInd);
										 DocOutputTypes.put((String)entry.getKey(), DocTypeMap);
										 
							    	 }
									 else if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("RTAG")&& SelectedInd)
							    	 {
										 HashMap<String, Boolean> DocTypeMap=DocOutputTypes.get(entry.getKey());
										 DocTypeMap.put(rs.getString("FDCIOBJECT_DESC"), SelectedInd);
										 DocOutputTypes.put((String)entry.getKey(), DocTypeMap);
										 
							    	 }
									 else if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("XBRL")&& SelectedInd)
							    	 {
										 HashMap<String, Boolean> DocTypeMap=DocOutputTypes.get(entry.getKey());
										 DocTypeMap.put(rs.getString("FDCIOBJECT_DESC"), SelectedInd);
										 DocOutputTypes.put((String)entry.getKey(), DocTypeMap);
										 
							    	 }
									 else if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("WORD")&& SelectedInd)
							    	 {		
										 HashMap<String, Boolean> DocTypeMap=DocOutputTypes.get(entry.getKey());
										 DocTypeMap.put(rs.getString("FDCIOBJECT_DESC"), SelectedInd);
										 DocOutputTypes.put((String)entry.getKey(), DocTypeMap);
							    	 }
									 else if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("EDGAR")&& SelectedInd)
							    	 {
										 HashMap<String, Boolean> DocTypeMap=DocOutputTypes.get(entry.getKey());
										 DocTypeMap.put(rs.getString("FDCIOBJECT_DESC"), SelectedInd);
										 DocOutputTypes.put((String)entry.getKey(), DocTypeMap);
										 
							    	 }
									 else if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("VPDF")&& SelectedInd)
							    	 {
										 HashMap<String, Boolean> DocTypeMap=DocOutputTypes.get(entry.getKey());
										 DocTypeMap.put(rs.getString("FDCIOBJECT_DESC"), SelectedInd);
										 DocOutputTypes.put((String)entry.getKey(), DocTypeMap);
										 
							    	 }
									 else if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("HTMLXBRL")&& SelectedInd)
							    	 {
										 HashMap<String, Boolean> DocTypeMap=DocOutputTypes.get(entry.getKey());
										 DocTypeMap.put(rs.getString("FDCIOBJECT_DESC"), SelectedInd);
										 DocOutputTypes.put((String)entry.getKey(), DocTypeMap);
										 
							    	 }
									 else if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("DOCSUMRPT")&& SelectedInd)
							    	 {
										 HashMap<String, Boolean> DocTypeMap=DocOutputTypes.get(entry.getKey());
										 DocTypeMap.put(rs.getString("FDCIOBJECT_DESC"), SelectedInd);
										 DocOutputTypes.put((String)entry.getKey(), DocTypeMap);
										 
							    	 }
									 else if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("SECTION PROOF")&& SelectedInd)
							    	 {
										 HashMap<String, Boolean> DocTypeMap=DocOutputTypes.get(entry.getKey());
										 DocTypeMap.put(rs.getString("FDCIOBJECT_DESC"), SelectedInd);
										 DocOutputTypes.put((String)entry.getKey(), DocTypeMap);
										 
							    	 }
									
									 group.setDocumentTypes(DocOutputTypes);
			                      }
								
							}
						}
					}
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				finally{
					try {
						if (con != null)
							con.close();
						if (rs != null)
							rs.close();
						if (cs != null)
							cs.close();
					} catch(SQLException e) {}	
				}

	             LOGGER.info("Exiting from com.dci.service.ServiceDao.getPrivileges(String userId, String clientId)");
		       return group;
	    }

}
