package com.dci.util;

import java.security.Key;
import java.security.SecureRandom;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.dci.model.Group;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

@Component("JwtToken")
@PropertySource(value = "classpath:application.properties")
public class JwtToken {
	private static final Logger LOGGER = LogManager.getLogger(JwtToken.class);
    
	@Autowired
	 private Environment env;
	
	public String getJwtToken(String userId, Map authenticationDetails, Map clientInfoMap,Group group)
	{
	LOGGER.info("Entering into com.dci.service.JwtToken.getJwtToken(String userId, Map authenticationDetails, Map clientInfoMap,Group group)");
		
		HashMap<String,Object> Headermap = new HashMap();
		HashMap<String,Object> claimsMap=new HashMap(); 
		
		// The JWT signature algorithm we will be using to sign the token
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		// We will sign our JWT with our ApiKey secret
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(env.getProperty("token.secretKey"));
		Key signingKey = new SecretKeySpec(apiKeySecretBytes,signatureAlgorithm.getJcaName());

		// setting the JWT Claims
		String internalUserID = authenticationDetails.get("HFUSERID").toString();
	
		//creating Random Number
		 String TokenId = UUID.randomUUID().toString();
		 String AppId=UUID.randomUUID().toString();
		
		 
		Headermap.put("alg", "HS256");
		Headermap.put("typ", "JWT");
		
		claimsMap.put("msgId",authenticationDetails.get("HFMSGID").toString());
		claimsMap.put("msg",authenticationDetails.get("HFMSG").toString());
		claimsMap.put("clientId",clientInfoMap.get("FDEPTID").toString());
		claimsMap.put("client_name",clientInfoMap.get("FDEPT_NAME").toString());
		claimsMap.put("Permissions", group);	
		
		int minutes=Integer.parseInt(env.getProperty("token.expTime"));
		Date exp=setTokenExpirationTime(minutes);
		long timeMilli = System.currentTimeMillis();
		Date now = new Date(timeMilli);
		String builder = Jwts.builder().setId(TokenId).setHeader(Headermap).setExpiration(exp).setIssuedAt(now)
				.setNotBefore(now).addClaims(claimsMap).signWith(SignatureAlgorithm.HS256, signingKey)
				.compact();
		
		LOGGER.info("Exiting from com.dci.service.JwtTokenServiceImpl.getJwtToken(String userId, Map authenticationDetails, Map clientInfoMap,Group group)");
		
		return builder;
		
	}
	
	public String getEmptyJwtToken(String msg)
	{
	LOGGER.info("Entering into com.dci.service.JwtToken.getJwtToken(String userId, Map authenticationDetails, Map clientInfoMap,Group group)");
		
	HashMap<String,Object> Headermap = new HashMap();
	HashMap<String,Object> claimsMap=new HashMap(); 
	claimsMap.put("Msg", msg);
		
		// The JWT signature algorithm we will be using to sign the token
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		// We will sign our JWT with our ApiKey secret
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(env.getProperty("token.secretKey"));
		Key signingKey = new SecretKeySpec(apiKeySecretBytes,signatureAlgorithm.getJcaName());

		// setting the JWT Claims
		String internalUserID = "123";
	
		//creating Random Number
		 String TokenId = UUID.randomUUID().toString();
		 String AppId=UUID.randomUUID().toString();
		 System.out.println("Application Id:"+AppId);
	
		int minutes=Integer.parseInt(env.getProperty("token.expTime"));
		Date exp=setTokenExpirationTime(minutes);
		long timeMilli = System.currentTimeMillis();
		Date now = new Date(timeMilli);
		String builder = Jwts.builder().setId(TokenId).setHeader(Headermap).setExpiration(exp).setIssuedAt(now)
				.setNotBefore(now).addClaims(claimsMap).signWith(SignatureAlgorithm.HS256, signingKey)
				.compact();
		
		LOGGER.info("Exiting from com.dci.service.JwtTokenServiceImpl.getJwtToken(String userId, Map authenticationDetails, Map clientInfoMap,Group group)");
		
		return builder;
		
	}
	
	
	
	
	
	
	public String getTokenId(String token) {
		 
		  String tokenid="";
		LOGGER.info("Entering into com.dci.service.JwtToken.getTokenId(String token)");
		
		try {
		   Claims claims =	Jwts.parser()
		              .setSigningKey(DatatypeConverter.parseBase64Binary(env.getProperty("token.secretKey")))
		              .parseClaimsJws(token).getBody();
		          Object permissions=claims.get("Permissions");
		          
		          tokenid=claims.get("jti").toString();
		          
		         
		}  catch(Exception e){
             e.printStackTrace();
		//System.out.println(" Some other exception in JWT parsing ");
		}
		System.out.println("tokenid = "+tokenid);
		LOGGER.info("Entering into com.dci.service.JwtToken.getTokenId(String token)");
		return tokenid;

		}
	public Date setTokenExpirationTime(int minutes)
	{
		int ExpTimeInMillis =minutes*60000;
		Date exp = null;
		long timeMilli = System.currentTimeMillis();
		//Date now = new Date(timeMilli);
		
		if (ExpTimeInMillis >= 0) {
			long expMillis = timeMilli + ExpTimeInMillis;
			exp = new Date(expMillis);

		}
		
		return exp;
	}
	
	
   public Object getTokenExpTime(String token) {
		
	   String msgreturn="";
	   
		LOGGER.info("Entering into com.dci.service.JwtToken.getDecodedToken(String token)");
		Object expTime = null;
		try {
		    Claims claims =	Jwts.parser()
			               .setSigningKey(DatatypeConverter.parseBase64Binary(env.getProperty("token.secretKey")))
			               .parseClaimsJws(token).getBody();
				           Object permissions=claims.get("Permissions");
				           expTime =claims.get("exp");
				           msgreturn="Token is alive";
		} catch (ExpiredJwtException e) {
			msgreturn="Token is expired";
		    //System.out.println(" Token expired ");
		} catch (SignatureException e) {
			msgreturn="Signature error";
		} catch(Exception e){
			msgreturn="error in JWT parsing";
			//System.out.println(" Some other exception in JWT parsing ");
		}
		
	    LOGGER.info("Entering into com.dci.service.JwtToken.getDecodedToken(String token)");
		return msgreturn;
		
		
	}
}
