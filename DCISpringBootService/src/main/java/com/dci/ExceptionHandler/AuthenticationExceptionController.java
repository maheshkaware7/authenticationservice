package com.dci.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.dci.model.ExceptionResponse;

@ControllerAdvice
public class AuthenticationExceptionController extends ExceptionHandlerExceptionResolver {

	/*
	 * @ExceptionHandler(value = {UserNotFoundException.class}) public
	 * ResponseEntity<Object> exception(UserNotFoundException
	 * exception,InvalidPasswordException pass) { return new ResponseEntity<>(pass,
	 * HttpStatus.NOT_FOUND); }
	 */
	
	 
	
     @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
     @ExceptionHandler(value=InvalidPasswordException.class)
	 public ResponseEntity<ExceptionResponse> handleInvalidPassword(InvalidPasswordException pass,HttpServletRequest request)
	 {
		 ExceptionResponse error = new ExceptionResponse();
			error.setErrorMessage(pass.getMessage());
			error.callerURL(request.getRequestURI());
			error.setErrorcode(HttpStatus.UNAUTHORIZED.value());
			
			return new ResponseEntity<ExceptionResponse>(error,HttpStatus.UNAUTHORIZED);
	 }
	 
	
	
     @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
     @ExceptionHandler(value=UserNotFoundException.class)
	 public  ResponseEntity<ExceptionResponse> handleInvalidUser(UserNotFoundException user,HttpServletRequest request)
	 {
		 //@ResponseBody ExceptionResponse
		 ExceptionResponse error = new ExceptionResponse();
			error.setErrorMessage(user.getMessage());
			error.callerURL(request.getRequestURI());
			error.setErrorcode(HttpStatus.UNAUTHORIZED.value());
			
			return new ResponseEntity<ExceptionResponse>(error,HttpStatus.NOT_FOUND);
	 }
	 
	
	 
     @ResponseStatus(value = HttpStatus.BAD_REQUEST)
     @ExceptionHandler(value =BadRequestException.class)
	 public ResponseEntity<ExceptionResponse> handleBadRequest(BadRequestException ex,HttpServletRequest request)
	 {
		 ExceptionResponse error = new ExceptionResponse();
		 error.setErrorMessage(ex.getMessage());
		 error.setErrorcode(HttpStatus.BAD_REQUEST.value());
		 error.callerURL(request.getRequestURI());
		 
		 return new ResponseEntity<ExceptionResponse>(error,HttpStatus.BAD_REQUEST);
	 }
	 
}
