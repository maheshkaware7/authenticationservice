package com.dci.ExceptionHandler;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
//import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class InvalidPasswordException extends Exception {

	private static final long serialVersionUID = -470180507998010368L;
	
	public InvalidPasswordException()
	{
		super();
	}

	public InvalidPasswordException(String exceptionString) {
		super(exceptionString);
		
	}
	
	
}
