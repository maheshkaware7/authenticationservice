package com.dci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SpringBootApplication
public class DciSpringBootServiceApplication{
	private static final Logger LOGGER = LogManager.getLogger(DciSpringBootServiceApplication.class);

	public static void main(String[] args) {
		LOGGER.info("Entering into main method of DciSpringBootServiceApplication");
		
		SpringApplication.run(DciSpringBootServiceApplication.class, args);
		LOGGER.info("url to access service:http://localhost:8080/auth/login/");
		LOGGER.info("url to access service in Swagger:http://localhost:8080/swagger-ui.html#/");
	}

}
