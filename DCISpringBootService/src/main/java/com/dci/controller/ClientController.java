package com.dci.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.session.Session;
import org.springframework.session.SessionRepository;
import org.springframework.session.web.http.HttpSessionManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.dci.ExceptionHandler.BadRequestException;
import com.dci.ExceptionHandler.InvalidPasswordException;
import com.dci.ExceptionHandler.UserNotFoundException;

import com.dci.dao.ServiceDao;
import com.dci.model.Group;
import com.dci.service.AuthenticationServiceImpl;


import com.dci.util.JwtToken;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@RestController
public class ClientController {
	private static final Logger LOGGER = LogManager.getLogger(ClientController.class);
	
	@Autowired
	private AuthenticationServiceImpl authServObj;
	
	@Autowired
	private JwtToken jwtToken;
	
	@Autowired
	private ServiceDao sdao;
	
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	private HashOperations<String, String, Object> hashOperations;

	@Autowired
	private Environment environment;
	
	List<String> ApllicationList=new ArrayList<String>(); 
	HashMap<String, List<String>> usermaplist=new HashMap<String, List<String>>();
	HashMap<String, List<String>> ApplicationUsersMap=new HashMap<String, List<String>>();
	@PostConstruct
	public void init()
	{
	hashOperations=redisTemplate.opsForHash();
	}

	@GetMapping("/DCI/clientpost/")
	public Object getClients() {

		return sdao.getClientList();
	}

	@PostMapping("/DCI/auth/login/")
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@ResponseBody
	public ResponseEntity<Object> login(@RequestParam("userId") String userId, @RequestParam("password") String password,
			@RequestParam("clientId") String clientId,@RequestParam("AppId") String AppId,HttpServletRequest request,HttpSession session)throws UserNotFoundException,InvalidPasswordException,BadRequestException{
		
		LOGGER.info(
				"Entering into com.dci.controller.ClientController.login(String userId, String password, String clientId)");
		String token="";
		if(!AppId.equals("10"))
		{
			String Emptytoken=jwtToken.getEmptyJwtToken("Token is for other than docubuilder application");
			System.out.println("Emptytoken = "+Emptytoken);
			token=Emptytoken;
			hashOperations.put(AppId, userId, token);// putting empty token into redis
			return null;
			
		}else
		{
		String data = null;
		Map<String,Object> resultMap=new HashMap();
		Map<String, Object> dataMap = new HashMap<String, Object>();
				
		if(!userId.equals("") && !password.equals("") && !clientId.equals("") && !AppId.equals(""))	
		{
			Map clientInfoMap =sdao.getClientInfo(userId, clientId);
			  String msg=(String)clientInfoMap.get("HFMSG");	  
			if(clientInfoMap!=null && !msg.contains("INVALID"))
			{					
				try {
					Map authenticationDetails = authServObj.authenticateUser(userId, password);
				
					if (authenticationDetails != null && authenticationDetails.get("HFMSGID") != null) {
		
						String msgId = authenticationDetails.get("HFMSGID").toString();
						if (msgId != null) {
							if ((msgId.trim().equals("-1") || msgId.trim().equals("2"))) {
								 data="Invalid password";
								 
							} else if ((msgId.trim().equals("-2") || msgId.trim().equals("3"))) {
								data = "Invalid user";
								
							} else if (msgId.trim().equals("5")) {
								data = "Force Change Password";
							} else if (msgId.trim().equals("6")) {
								data = "User locked";
							} else if (msgId.trim().equals("99")) {
								data = "User/password expired";
							} else if (msgId.trim().equals("7")) {
								data = "User deactivated";
							} else if (msgId.trim().equals("0")) {
								data = "success";
								
								 String InternalId =authenticationDetails.get("HFUSERID").toString();
								  Group group=(Group) sdao.getPrivileges(InternalId,userId,clientId);
									token = jwtToken.getJwtToken(userId, authenticationDetails, clientInfoMap,group);
									String tokenId=jwtToken.getTokenId(token);
									System.out.println("AppId = "+AppId);
									hashOperations.put(AppId,userId,token);
									System.out.println("hasoperations get = "+hashOperations.get(AppId, userId));
							      //  Object Permission=jwtToken.getDecodedToken(token);
									dataMap.put("Token", token);
									//dataMap.put("Permissions",Permission);
								}
							}
						}
						
					}
				    catch(Exception e){
				    	
				    	e.printStackTrace();
				    }
				
			}
			else {
				
				//dataMap.put("msg",clientInfoMap.get("HFMSG").toString());
				data=(String) clientInfoMap.get("HFMSG");
				 throw new UserNotFoundException("User Not Found/Invalid User");
			}
			  
			 if(data.equalsIgnoreCase("Success"))
			 {
				// dataMap.put("message", data);
				 resultMap.put(data, dataMap);
			 }
			else
			{
				dataMap.put("message", data);
				resultMap.put("Failed", dataMap);
				throw new InvalidPasswordException("Invalid password"); 
			}
		LOGGER.info("Exiting from com.dci.controller.ClientController.login(String userId, String password, String clientId)");
		return new ResponseEntity<Object>(resultMap, HttpStatus.OK);
		}
		else {
			 resultMap.put("Failed","Please enter vaild input");
			 throw new BadRequestException("Please enter vaild input");
			
			 
		}
	}
		
	 }
	
	
	@PostMapping("/DCI/auth/verifyuser")
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public Object VerifyUser(@RequestParam("userId") String userId,@RequestParam("tokenId") String tokenId,@RequestParam("AppId") String AppId) {
	LOGGER.info(
	"Entering into com.dci.controller.ClientController.verifyuser(String userId, String clientId)");

	//System.out.println("Check Token: "+redisTemplate.opsForHash().hasKey("10", tokenId));
	
	// fetching token according to requested appid,userid 
	String token=(String) hashOperations.get(AppId,userId);
	
	
	// if token is null then return msg
	if(token==null)
	{
		return "Token Doesn't Exists";
	}
	else
	{
		System.out.println("UserId = "+userId+"Uses "+AppId+" Application");
		
		List<String> userList=new ArrayList<String>();
		if(!ApplicationUsersMap.containsKey(AppId))
		{
			
			userList.add(userId);
			ApplicationUsersMap.put(AppId, userList);
		}else if(ApplicationUsersMap.containsKey(AppId))
		{
			userList=ApplicationUsersMap.get(AppId);
			userList.add(userId);
			// map to put userlist against that particular application			
			ApplicationUsersMap.put(AppId, userList);
			
			
		}
		
		// 
		 for(Map.Entry m:ApplicationUsersMap.entrySet())
		 {
			 
			  System.out.println("ApplicationUsersMap = "+m.getKey()+":::"+m.getValue());
			  
		 }
		 
		long exptime = 0;
		
		String msg=jwtToken.getTokenExpTime(token).toString();
		    long timeMilli = System.currentTimeMillis();
		    System.out.println("msg = "+msg);
		 if(msg.contains("alive"))
		 {
			 return token;
		 }
		 else
		 {
			 return msg;
		 }

		
	}
}
	
	@PostMapping("/DCI/tokenpost/")
	public Object getToken(@RequestParam String token) {

	final String uri = "http://192.168.1.23:8080/jwt/{token}";

	Map<String, String> params = new HashMap<String, String>();
	params.put("token", token);

	RestTemplate restTemplate = new RestTemplate();
	String result = restTemplate.getForObject(uri, String.class, params);
	if (result.equalsIgnoreCase("valid")) 
	{
	   return null;//sdao.getClients();
	}
	else {
	  return null;
	}

	}
	
	 @GetMapping("/DCI/uId/")
	    String uid(HttpSession session) {
	        return session.getId();
	    }
	 
	 
	

}
	
	

